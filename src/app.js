const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const router = require('./router')
const auth = require('./auth')

app.use(express.json())
app.use(express.urlencoded())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(router)
app.use('/wx/auth', auth)
function error_handler(err, req, res, next) {
  if (err) {
    res.status(500).json({
      message: err.message,
    })
  }
}
app.use(error_handler)

function not_found(req, res, next) {
  res.json({
    message: 'api不存在',
  })
}
app.use(not_found)

app.listen(3000)
