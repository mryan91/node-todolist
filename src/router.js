const express = require('express')
const router = express.Router()

const models = require('../db/models')
const { SuccessModel, ErrorModel } = require('./resModel')

//查询任务列表
router.get('/selectPage', async (req, res, next) => {
  // 状态： 1 待办，2 完成， 3 删除
  try {
    console.log(req)
    const { status, page = 1 } = req.query
    const limit = 10
    const where = {}
    if (status) {
      where.status = status
    }
    const todo = await models.Todo.findAndCountAll({
      where,
      offset: Number((page - 1) * limit),
      limit: 10,
    })
    const a = res.json({
      ...new SuccessModel(todo, '查询成功'),
    })
  } catch (error) {
    next(error)
  }
})

// 新建,修改
router.post('/saveOrUpdate', async (req, res, next) => {
  try {
    const { name, toTime, content, status, id } = req.body
    if (id) {
      const todo = await models.Todo.update(
        {
          name,
          toTime,
          content,
          status,
        },
        {
          where: {
            id,
          },
        },
      )
      if (!todo) {
        res.json({
          ...new SuccessModel('未找到'),
        })
        return
      }

      res.json({
        ...new SuccessModel(todo, '修改成功'),
      })
    } else {
      const todo = await models.Todo.create({
        name,
        toTime,
        content,
      })
      res.json({
        ...new SuccessModel(todo, '创建成功'),
      })
    }
  } catch (error) {
    next(error)
  }
})

// 删除
router.get('/delete/:id', async (req, res, next) => {
  const { id } = req.params
  const newStatus = await models.Todo.update(
    {
      status: 3,
    },
    {
      where: {
        id,
      },
    },
  )
  res.json({
    ...new SuccessModel(newStatus, '删除成功'),
  })
})

// 更新状态
router.post('/updateStatus', async (req, res, next) => {
  const { status, id } = req.body
  const newStatus = await models.Todo.update(
    {
      status,
    },
    {
      where: {
        id,
      },
    },
  )
  res.json({
    ...new SuccessModel(newStatus, '更新状态成功'),
  })
})

module.exports = router
