const crypto = require('crypto')
const token = 'teary1989'

function sha1(str) {
  const shasum = crypto.createHash('sha1')
  return shasum.update(str, 'utf-8').digest('hex')
}

function wechatAuth(req, res) {
  const { signature, echostr, timestamp, nonce } = req.query

  let reqArray = [nonce, timestamp, token].sort()
  let sortStr = reqArray.join('')
  let sha1Str = sha1(sortStr.toString().replace(/,/g, ''))
  if (signature === sha1Str) {
    res.end(echostr)
  } else {
    res.end('false')
    console.log('授权失败!')
  }
}

module.exports = wechatAuth
