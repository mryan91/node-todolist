## 需求

- 条件查询任务列表，并分页
- 增删改查
- 修改状态
  - id
  - 状态值（待办/完成)

## 接口实现

### 数据库操作

#### 数据库初始化

- 创建数据库

- 使用 `sequelize cli` 初始化项目数据库配置信息

  ```shell
  npx sequelize init
  ```

- 创建模型

  ```shell
  npx sequelize model:generate --name Todo	--attributes name:string,deadline:timestamp,content:string
  ```

- 数据库建表

  ```shell
  npx sequelize db:migrate
  ```

- 发布

```
  npm install pm2 -g
  pm2 init
  pm2 start ecosystem.config.js
  pm2 log
  pm2 restart ecosystem.config.js

```
